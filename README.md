Każde zadanie jest na innym branchu

uwagi do zadań:

zad1: java,kotlin i python

zad2: jeden kotroler bazujący na listach z pełnym CRUD  
aplikacja działa na porcie 9000  
dołączona jest bardzo prosta strona żeby potestować GET i POST  

zad3: bot obsługuje komendy na discordzie i może zwracać listy produktów według podanej kategorii, jest dodany screen z botem w akcji  
link do bota: https://discord.com/api/oauth2/authorize?client_id=1097739314835750932&permissions=3072&scope=bot

zad4: tylko obsługa CRUD z kontrolerem bazującym na listach  
aplikacja działa na porcie 1323

zad5: aplikacja bazuje na zadaniu 4 czyli jest go + react port 1323  
trzy komponenty: produkty, koszyk i płatności dane między nimi przesyłane są przy pomocy hooków.  
W zasadzie to nie wiem co miały dokładnie robić płatności, ale tutaj po prosty wysyłają sumę z koszyka na serwer i tam jest ona po prostu dodawana, pod ścieżką /kasa można sprawdzić tą sumę na serwerze.  

zad6: projekt z zadania5, ale przerobiony, aby używać tylko Next.js i testy w CypressJS  + docker-compose, który uruchamia aplikacje i testy
aplikacja działa na porcie 3000  

zad7: projektz zadania6 z dodanym eslintem  
na innym repozytorium na githubie są odznaki sonara  
https://github.com/Revikatis/sonar

zad8: rozwinięty projekt z zadania5,port 1323, tylko logowania dla kont:  
{Email: "admin@mail.com", Password: "admin"},{Email: "example@mail.com",Password: "1234"}

zad9: projekt z zadania3 z dodaną komendą !gpt, która pozwala na wysyłanie wiadomości do chatGPT

zad10:projekt z zadania5 na chmurze Azure, kod na innym repozytorium na githubie  
https://github.com/Revikatis/nextjs-app  
https://happy-ocean-0601a7003.3.azurestaticapps.net/  
